// Dependencies and Modules

const User = require("../models/User");
const Product = require('../models/Product');
const Order = require("../models/Order");
const bcrypt = require ('bcrypt');
const auth = require('../auth');

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {

		// The "find" metho
		// returns a record if a match is found.
		if(result.length > 0) {
			return true; //"Duplicate email found"
		} else {
			return false;
		}
	})
};

module.exports.registerUser = (req, res) => {

	// Creates a variable newUser and instantiates a new User object using the mongoose model.
	// Uses the information from the request body to provide all the necessary information about the User object.
	let newUser = new User({
		email: req.body.email,
		// 10 - is the number of salt rounds that bcrypt algorithm will run in order to encrypt the password.
		password: bcrypt.hashSync(req.body.password, 10)
	})

	// Saves the created object to our database
	return newUser.save().then((user, error) => {
		// User registratioon failed
		if(error) {
			return res.send(false);
		// User registration successful
		} else {
			return res.send(true);
		}
		// catch() code block will handle other kinds of error.
		// prevent our app from crashing when an error occured in the backend server.
	}).catch(err => res.send(err));

};

module.exports.loginUser = (req, res) => {
	return User.findOne({email: req.body.email}).then(result => {
		// User does not exist
		if(result == null ) {
			return res.send(false);
		} else {
			// Created the isPasswordCorrect variable to return the result of comparing the login from password and the database password
			// compareSync() method is used to compare the non-encrypted password from the login form to the encrypted password from the database
				// will return either "true" or "false"
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password)

			if(isPasswordCorrect){
				// Generate an accecss token
				// Uses the "createAccessToken" method defined in the "auth.js" file.
				// Returning an object back to the front end application is a common practice to ensure that information is properly labled and real world examples normally return more complex information represented by objects.
				return res.send({access: auth.createAccessToken(result), message: 'Successfully login'})
			} else {
				return res.send(false);
			}
		}
	}).catch(err => res.send(err))
}

module.exports.getProfile = (req, res ) => {
	return User.findById(req.user.id).then( result => {
		// console.log(result)
		result.password = "";
		return res.send(result);
	}).catch(err => res.send(err))
};

module.exports.updateUserAdmin = async (req, res) => {
  try {
    const { userId } = req.body;

    // Check if the provided user ID exists
    const user = await User.findById(userId);
    if (!user) {
      return res.status(404).json({ message: 'User not found' });
    }

    // Update the user as an admin
    user.isAdmin = true;
    await user.save();

    return res.json({ message: 'User updated as admin successfully' });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: 'Internal server error' });
  }
};

module.exports.removeUserAdmin = async (req, res) => {
  let updateActive = {
    isAdmin: false
  }

  return User.findByIdAndUpdate(req.params.userId , updateActive).then((user, error) =>{
    console.log(user)
    if(error){
      return res.send(false);
    } else {
      return res.send(true);
    }
  }).catch(err => res.send(err));
};

module.exports.getUserOrders = async (req, res) => {
  try {
    const userId = req.user.id; 

    // Retrieve the user's orders from the database
    const orders = await Order.find({ userId: userId }).populate('products');
    console.log(orders);
    res.json(orders);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal Server Error' });
  }
};

module.exports.resetPassword = async (req, res) => {
  try {
    const { newPassword } = req.body;
    const { id } = req.user; // Extracting user ID from the authorization header

    // Hashing the new password
    const hashedPassword = await bcrypt.hash(newPassword, 10);

    // Updating the user's password in the database
    await User.findByIdAndUpdate(id, { password: hashedPassword });

    // Sending a success response
    res.status(200).json(true);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal server error' });
  }
};


module.exports.updateProfile = async (req, res) => {
  try {
    // Get the user ID from the authenticated token
    const userId = req.user.id;

    // Retrieve the updated profile information from the request body
    const { email } = req.body;

    // Update the user's profile in the database
    const updatedUser = await User.findByIdAndUpdate(
      userId,
      { email },
      { new: true }
    );

    res.json(updatedUser);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Failed to update profile' });
  }
};

module.exports.getAllUser = (req, res) => {
  return User.find({}).then(result => {
    // console.log(result)
    return res.send(result)
  }).catch (err => res.send(err));
}


module.exports.updateUser= (req , res) => {
  let updatedUser = { 
    email: req.body.email,
    isAdmin: req.body.isAdmin
  }

  return User.findByIdAndUpdate(req.params.userId , updatedUser).then((user, error) => {
    // console.log(product);
    if(error){
      return res.send(false);
    } else {
      return res.send(true);
    }
  }).catch(err => res.send(err))
}

module.exports.getUser = (req, res) => {
  return User.findById(req.params.userId).then(result =>  {
    console.log(result)
    return res.send(result);
  }).catch(err => res.send(err));
}