const Product = require('../models/Product');


module.exports.addProduct = (req, res) => {
	// const { name , description, price } = req.body;
	// const productImage = req.file.filename;

	// let newProduct = new Product({
	// 	name,
	// 	description,
	// 	price,
	// 	productImage
	// })

	let newProduct = new Product({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	})

	return newProduct.save().then((product, error ) => {
		if(error){
			return res.send(false)
		} else {
			return res.send(true)
		}
	}).catch(err => res.send(err))
}

module.exports.getAllProducts = (req, res) => {
	return Product.find({}).then(result => {
		// console.log(result)
		return res.send(result)
	}).catch (err => res.send(err));
}

module.exports.getAllActive = (req, res) => {
	return Product.find({isActive : true }).then(result => {
		// console.log(result)
		return res.send(result);
	}).catch (err => res.send(err));
}

module.exports.getProduct = (req, res) => {
	return Product.findById(req.params.productId).then(result =>  {
		console.log(result)
		return res.send(result);
	}).catch(err => res.send(err));
}

module.exports.updateProduct = (req , res) => {
	let updatedProduct = { 
		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	return Product.findByIdAndUpdate(req.params.productId , updatedProduct).then((product, error) => {
		// console.log(product);
		if(error){
			return res.send(false);
		} else {
			return res.send(true);
		}
	}).catch(err => res.send(err))
}

module.exports.archiveProduct = (req, res) => {
	let updatedActive = {
		isActive: false
	}

	return Product.findByIdAndUpdate(req.params.productId , updatedActive).then((product, error) =>{
		console.log(product);
		if(error){
			return res.send(false);
		} else {
			return res.send(true);
		}
	}).catch(err => res.send(err));
}

module.exports.activateProduct = (req, res) => {
	let updatedActive = {
		isActive: true
	}

	return Product.findByIdAndUpdate(req.params.productId , updatedActive).then((product, error) =>{
		if(error){
			return res.send(false);
		} else {
			return res.send(true);
		}
	}).catch(err => res.send(err));
}

module.exports.searchProductByName = async (req, res) => {
  try {
    const { productName } = req.body;

    // Use a regular expression to perform a case-insensitive search
    const product = await Product.find({
      name: { $regex: productName, $options: 'i' }
    });

    res.json(product);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};

module.exports.searchProductByPriceRange = async (req, res) => {
     try {
       const { minPrice, maxPrice } = req.body;
  
          // Find product within the price range
       const product = await Product.find({
        price: { $gte: minPrice, $lte: maxPrice }
      });
      
      res.status(200).json({ product });
    } catch (error) {
      res.status(500).json({ error: 'An error occurred while searching for product' });
    }
   };