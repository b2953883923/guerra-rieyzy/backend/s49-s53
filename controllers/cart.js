const Cart = require('../models/Cart');
const Product = require('../models/Product');


module.exports.addToCart = async (req , res ) => {
  try {
      const { productId, quantity } = req.body;
      const userId = req.user.id; 

      if (req.user.isAdmin) {
        return res.status(403).json({ message: 'Action Forbidden' });
      }

      // Check if the product exists
      const product = await Product.findById(productId);
      if (!product) {
        return res.status(404).json({ error: 'Product not found' });
      }

      // Calculate the total amount based on product price and quantity
      const subTotal = product.price * quantity;

      // Create a new cart
      const cart = new Cart({
        products: [
          {
            productId: productId,
            quantity: quantity,
            subTotal
          }
        ],
        userId: userId
      });

      // Save the order to the database
      await cart.save();

      res.status(201).json({ message: 'Added to cart successfully' });
    } catch (error) {
      // console.error(error);
      res.status(500).json({ error: 'Server error' });
  }
};


module.exports.updateCartQuantity = async (req, res) => {
  try {
    const { productId, quantity } = req.body; // Assuming productId and quantity are sent in the request body
    const user = await User.findById(req.user._id); // Assuming user is authenticated and user ID is available in req.user

    if (req.user.isAdmin) {
     return res.status(403).json({ message: 'Action Forbidden' });
     }

    if (!user) {npm
      return res.status(404).json({ error: 'User not found' });
    }

    if (!cart) {
      // Create a new cart if it doesn't exist
      cart = new Cart({ userId });
    }

    const productIndex = cart.products.find(
      (item) => item.product.toString() === productId
    );

    if (productIndex === -1) {
      return res.status(404).json({ error: 'Product not found in cart' });
    }

    if (quantity <= 0) {
      // If quantity is zero or negative, remove the product from the cart
      cart.products.splice(productIndex, 1);
    } else {
      // Update the quantity of the product in the cart
      cart.products[productIndex].quantity = quantity;
    }

    await cart.save();

    return res.status(200).json(cart);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Server Error' });
  }
};


module.exports.getCart = async (req, res) => {
  const userId  = req.user.id;

    try {
      // Query the database to fetch all orders of the user
      const carts = await Cart.find({ userId })


      res.json({ carts });
    } catch (error) {
      console.error(error);
      res.status(500).json({ success: false, message: 'Internal Server Error' });
    }
};

exports.removeFromCart = async (req, res) => {
  const { productId } = req.params;
  try {
    const cart = await Cart.findOne({ userId: req.user._id });
    if (!cart) {
      return res.status(404).json({ error: 'Cart not found' });
    }

    // Remove the product from the products array based on productId
    cart.products = cart.products.filter((item) => item.productId !== productId);
    await cart.save();
    res.json(cart);
  } catch (error) {
    res.status(500).json({ error: 'Error removing product from cart' });
  }
};


module.exports.createOrder = async (req, res) => {
  try {
    const { userId } = req.user;

    const cart = await Cart.findOne({ user: userId });

    if (!cart || cart.products.length === 0) {
      return res.status(404).json({ message: 'Cart is empty.' });
    }

    cart.products = [];
    await cart.save();

    res.json({ message: 'Order placed successfully.' });
  } catch (error) {
    console.error('Error ordering from cart:', error);
    res.status(500).json({ message: 'An error occurred while ordering from cart.' });
  }
};