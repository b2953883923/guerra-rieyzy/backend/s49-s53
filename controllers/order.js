const Product = require('../models/Product');
const Order = require("../models/Order");

module.exports.createOrder = async (req , res ) => {
	try {
	    const { productId, quantity } = req.body;
	    const userId = req.user.id; 

	    if (req.user.isAdmin) {
	      return res.status(403).json({ message: 'Action Forbidden' });
	    }

	    // Check if the product exists
	    const product = await Product.findById(productId);
	    if (!product) {
	      return res.status(404).json({ error: 'Product not found' });
	    }

	    // Calculate the total amount based on product price and quantity
	    const totalAmount = product.price * quantity;

	    // Create a new order
	    const order = new Order({
	      products: [
	        {
	          productId: productId,
	          quantity: quantity,
	        }
	      ],
	      userId: userId,
	      totalAmount,
	    });

	    // Save the order to the database
	    await order.save();

	    res.status(201).json({ message: 'Order created successfully' });
	  } catch (error) {
	    // console.error(error);
	    res.status(500).json({ error: 'Server error' });
  }
};

module.exports.getAllOrders = (req, res ) => {
	return Order.find({}).then(result => {
		console.log(result)
		return res.send(result)
	}).catch (err => res.send(err));
}


module.exports.getOrdersByUser = async (req, res) => {
  const userId  = req.user.id;

    try {
      // Query the database to fetch all orders of the user
      const orders = await Order.find({ userId })


      res.json({ orders });
    } catch (error) {
      console.error(error);
      res.status(500).json({ success: false, message: 'Internal Server Error' });
    }
};