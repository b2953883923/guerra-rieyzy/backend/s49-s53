const mongoose = require('mongoose');

const cartSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true , 'User ID is required']
	},
	products: [
		{
			productId: {
				type: String,
				required: [true , 'Product ID is required']
			},
			quantity: {
				type: Number,
				default: 1
			},
				subTotal: {
				type: Number,
				required: [true , 'Subtotal is required']
			}
		}
	],
	createAt: {
		type: Date,
		default: new Date()
	}
})




module.exports = mongoose.model("Cart", cartSchema);