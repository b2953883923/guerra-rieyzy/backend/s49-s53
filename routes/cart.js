const express = require('express');
const cartController = require('../controllers/cart');
const auth = require('../auth');

const {verify, verifyAdmin} = auth;

const router = express.Router();

router.get("/cart", verify, cartController.getCart);

router.post("/addToCart", verify, cartController.addToCart);

router.post("/order", verify, cartController.createOrder);

// router.put("/:productId", verify, cartController.updateCartQuantity);

router.delete("/:productId", verify, cartController.removeFromCart);




module.exports = router;