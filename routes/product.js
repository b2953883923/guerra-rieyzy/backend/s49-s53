const express = require('express');
const productController = require('../controllers/product');
const auth = require('../auth');
const upload = require('../multer');

const {verify, verifyAdmin} = auth;

const router = express.Router();

//router.post("/new-product", verify, verifyAdmin ,upload.single('productImage'), productController.addProduct);

router.post("/new-product", verify, verifyAdmin ,upload.single('productImage'), productController.addProduct);

router.get("/all", productController.getAllProducts);

router.get("/", productController.getAllActive);

router.post('/search', productController.searchProductByName);

router.post('/searchByPrice', productController.searchProductByPriceRange);

router.get("/:productId", productController.getProduct);

router.put("/:productId", verify, verifyAdmin, productController.updateProduct);

router.put("/:productId/archive", verify, verifyAdmin, productController.archiveProduct);

router.put("/:productId/activate", verify, verifyAdmin, productController.activateProduct);

module.exports = router;