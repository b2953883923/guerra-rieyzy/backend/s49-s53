// Dependencies and Modules
const express = require('express');
const userController = require('../controllers/user');
const auth = require('../auth');

// destructure the auth file:
// here we use the verify and verifyAdmin as auth middlewares
const {verify, verifyAdmin} = auth;

const router = express.Router();

// router.post("/checkEmail", userController.checkEmailExists);

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

router.post("/register", userController.registerUser);

router.post("/login", userController.loginUser);

router.get("/details", verify, userController.getProfile);

router.get('/getOrders',verify, userController.getUserOrders);

router.put('/profile', verify, userController.updateProfile);

router.put('/reset-password', verify, userController.resetPassword);

router.get('/', userController.getAllUser);

router.put('/:usersId/new-admin', verify, verifyAdmin, userController.updateUserAdmin);

router.put('/:usersId/remove-admin', verify, verifyAdmin, userController.removeUserAdmin);

router.get("/:userId", userController.getUser);

router.put("/:userId", verify, verifyAdmin, userController.updateUser);

module.exports = router;