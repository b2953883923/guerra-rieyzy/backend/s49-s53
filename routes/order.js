const express = require('express');
const orderController = require('../controllers/order');
const auth = require('../auth');

const {verify, verifyAdmin} = auth;

const router = express.Router();


router.post("/", verify, orderController.createOrder);

router.get("/all", verify, verifyAdmin, orderController.getAllOrders);

router.get("/user", verify, orderController.getOrdersByUser)


module.exports = router;