// Express Server Dependencies
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const userRoutes = require("./routes/user");
const productRoute = require("./routes/product");
const orderRoute = require("./routes/order");
const cartRoute = require("./routes/cart");
const multer = require('multer');
const path = require('path');
const uuidv4 = require('uuid/v4');

// Server Setup
const app = express();

// Environment Setup
const port = 3001;

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Allows all resources(frontend app) to access our backend application.
app.use(cors());


mongoose.connect("mongodb+srv://rieyzyguerra:admin123@batch295.kxfkqps.mongodb.net/capstone2-API?retryWrites=true&w=majority", 
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		}
	);


let db = mongoose.connection;
db.on('error', console.error.bind(console, 'Connection Error'));
db.once('open', () => console.log('Connected to MongoDB Atlas.'));

// Backend Routes
// http://localhost:3000/users
app.use("/users", userRoutes);
app.use("/products", productRoute);
app.use("/orders", orderRoute);
app.use("/carts", cartRoute);


// Server listening to the port.
if(require.main === module) {
	app.listen(process.env.PORT || port, () => {console.log(`Server is now running in port ${process.env.PORT || port}.`)});
}

// Export the app server if the the task to be performed is not found in the main module.
module.exports = {app, mongoose};